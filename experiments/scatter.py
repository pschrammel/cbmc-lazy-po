#!/usr/bin/python

import csv
import matplotlib.pyplot as plt
import sys


def num(s):
    try:
        return float(s)
    except ValueError:
        return 0.0


file1=sys.argv[1]
file2=sys.argv[2]
col=int(sys.argv[3])

print ("Scatter plot of files %s %s col %d " % (file1, file2, col))

table1 = csv.reader(open(file1), delimiter='&', lineterminator="\\\\\r\n",
                    quotechar=' ', quoting=csv.QUOTE_MINIMAL)   

table2 = csv.reader(open(file2), delimiter='&', lineterminator="\\\\\r\n",
                    quotechar=' ', quoting=csv.QUOTE_MINIMAL)   

x=[num(row[col]) for row in table1]
y=[num(row[col]) for row in table2]

print x
print y




f, ax = plt.subplots(figsize=(8, 8))


# add a 'best fit' line
plt.xlabel('')
plt.ylabel('')
plt.title(r'')

maximum=max(max(x),max(y))+10

plt.xlim(0, maximum)
plt.ylim(0, maximum)


# Tweak spacing to prevent clipping of ylabel
scatter=plt.scatter(x, y, s=1)


ax.scatter(x, y, c=".3")
ax.set(xlim=(0, maximum), ylim=(0, maximum))

# Plot your initial diagonal line based on the starting
# xlims and ylims.
diag_line, = plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")

def on_change(axes):
    # When this function is called it checks the current
    # values of xlim and ylim and modifies diag_line
    # accordingly.
    x_lims = ax.get_xlim()
    y_lims = ax.get_ylim()
    diag_line.set_data(x_lims, y_lims)

# Connect two callbacks to your axis instance.
# These will call the function "on_change" whenever
# xlim or ylim is changed.
ax.callbacks.connect('xlim_changed', on_change)
ax.callbacks.connect('ylim_changed', on_change)

plt.show()

