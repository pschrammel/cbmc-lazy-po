#!/bin/bash


for t in vanilla lazy mus
do
for m in tso pso sc
do
rm -f performance-${t}-${m}.txt
for d in pthread-atomic pthread wmm-core
do
cd ${d}/logcbmc-${t}-${m}
../../analyse.py
cp performance.csv _performance.csv
[ ${d} != "pthread-atomic" ] && (tail -n +2 performance.csv > _performance.csv)
cat _performance.csv >> ../../performance-${t}-${m}.txt
rm _performance.csv
cd ../..
done
sed -i.bak -E 's/s&/\t/g' performance-${t}-${m}.txt
sed -i.bak -E 's/&/\t/g' performance-${t}-${m}.txt
sed -i.bak -E 's/\\//g' performance-${t}-${m}.txt
done
done

rm *.bak
