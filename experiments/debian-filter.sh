cd debian-pthread
for f in *.goto 
do 
  echo $f
   ../../trunk/src/cbmc/cbmc-re $f --show-goto-functions | grep ASSERT > /dev/null
  if [ $? -eq 0 ] 
  then
    echo $f "has_assertion"
    perl -e 'alarm shift @ARGV; exec @ARGV' 300 "../../trunk/src/cbmc/cbmc-re $f --show-vcc --no-unwinding-assertions --unwind 1 | grep VCC > tmp.txt"
    if [ $? -eq 0 ]
    then
      grep 0 tmp.txt > /dev/null
      [ $? -eq 1 ] && echo $f "has_reachable_assertion"
    fi
  fi
done
rm tmp.txt
