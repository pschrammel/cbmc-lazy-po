#!/bin/bash

nohup ./run_cbmc_vanilla.sh
nohup ./run_cbmc_vanilla_tso.sh
nohup ./run_cbmc_vanilla_pso.sh
nohup ./run_cbmc_lazy.sh
nohup ./run_cbmc_lazy_tso.sh
nohup ./run_cbmc_lazy_pso.sh

