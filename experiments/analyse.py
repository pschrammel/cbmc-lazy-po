#!/usr/bin/python

import os
import sys
import re

import csv

benchmark_name=re.compile('Parsing')
cat=re.compile('Nr of thread categories')

def package(path):
  path_vec=path.split('/')
  package=path_vec[1]
  return package

FILE_SEPARATOR="%===%"


file_sep=re.compile(FILE_SEPARATOR)

po_total_str="Partial order constraints used:"

po_total=re.compile(po_total_str)
fr=re.compile("fr:")             # fr: 292/1609
po=re.compile("po:")            # po: 105/105
rf=re.compile("rf:")             # rf: 171/238
rf_order=re.compile("rf-order:") # rf-order 128/238
rf_some=re.compile("rf-some:")   # rf-some: 44/53
rfi=re.compile("rfi:")           # rfi: 35/49
thread_spawn=re.compile("thread-spawn:") # thread-spawn: 3/3
ws_ext=re.compile("ws-ext:")      # ws-ext: 84/152

total_time=re.compile("Time:")
sat_time=re.compile("Runtime decision procedure: ")
iterations=re.compile("Total iterations: ")
vars=re.compile("variables")

def read_ratio(str):
  cols=str.split("/")
  result=float(cols[0])/ float(cols[1])
  return " {:.2f} ".format(result)

def read_tuple(str):
  cols=str.split("/")
  return (int(cols[0]),int(cols[1]))


# statistics about constraints used 
def build_constraints_table(content):
  depth=re.compile('Depth:')

  report_file=open("ratios.csv", 'wb')
  report = csv.writer(report_file, delimiter='&', lineterminator="\\\\\r\n",
                              quotechar=' ', quoting=csv.QUOTE_MINIMAL)   
  depth_list=[]
  benchmark=""

  po_total_nr=0
  po_total_val=""
  fr_val=""
  po_val=""
  rf_val=""
  rf_order_val=""
  rf_some_val=""
  rfi_val=""
  thread_spawn_val=""
  ws_ext_val=""

  report.writerow(
   ["benchmark",
    " total ",
    " fr ",
    " po ",
    " rf ",
    " rf-order ",
    " rf-some ",
    " rfi ",
    " thread-spawn ",
    " ws-ext "])

  for line in content:
    cols=line.split()  

    if benchmark_name.search(line):
      benchmark=cols[1]
      pos=benchmark.rfind("/")
      benchmark=benchmark[pos+1:]
      benchmark=benchmark.replace('_', '\_')

    if po_total.search(line):
      po_total_val=read_ratio(cols[4])
      po_total_tuple=read_tuple(cols[4])
      po_total_nr=po_total_tuple[1]

    if fr.search(line):
      fr_val=read_ratio(cols[1])

    if po.search(line):
      po_val=read_ratio(cols[1])

    if rf.search(line):
      rf_val=read_ratio(cols[1])

    if rf_order.search(line):
      rf_order_val=read_ratio(cols[1])

    if rf_some.search(line):
      rf_some_val=read_ratio(cols[1])

    if rfi.search(line):
      rfi_val=read_ratio(cols[1])
    
    if thread_spawn.search(line):
      thread_spawn_val=read_ratio(cols[1])

    if ws_ext.search(line):
      ws_ext_val=read_ratio(cols[1])

    if file_sep.search(line):

      report.writerow(
        [benchmark,
         po_total_nr,
         po_total_val,
         fr_val,
         po_val,
         rf_val,
         rf_order_val,
         rf_some_val,
         rfi_val,
         thread_spawn_val,
         ws_ext_val])


# statistics about constraints used 
def build_performance_table(content):

  report_file=open("performance.csv", 'wb')
  report = csv.writer(report_file, delimiter='&', lineterminator="\\\\\r\n",
                              quotechar=' ', quoting=csv.QUOTE_MINIMAL)   
  depth_list=[]
  benchmark=""

  total_time_val=0.0  # total running time in seconds
  sat_time_val=0.0    # SAT running time in seconds
  iterations_val=0  # nr of refinements
  vars_val=0        # nr of variables in final SAT instance
  clauses_val=0     # nr of clauses in final SAT instance
  used_val=0   # nr of constraints used
  total_val=0  # nr of constraints in total


  report.writerow(
   ["benchmark",
    " total time ",
    " sat time",
    " iters ",
    " var ",
    " cl ",
    " used ",
    " total "])

  for line in content:
    cols=line.split()  

    if benchmark_name.search(line):
      benchmark=cols[1]
      pos=benchmark.rfind("/")
      benchmark=benchmark[pos+1:]
      benchmark=benchmark.replace('_', '\_')

    if total_time.search(line):
      total_time_val=cols[1]

    if sat_time.search(line):
      sat_time_val=cols[3]    

    if iterations.search(line):
      iterations_val=cols[2]
      
    if(vars.search(line)):
      vars_val=cols[0]
      clauses_val=cols[2]

    if po_total.search(line):
      po_total_tuple=read_tuple(cols[4])
      used_val=po_total_tuple[0]
      total_val=po_total_tuple[1]

    if file_sep.search(line):
      report.writerow(
        [benchmark,
         total_time_val,
         sat_time_val,
         iterations_val,
         vars_val,
         clauses_val,
         used_val,
         total_val])


benchmarks = [os.path.join(dp, f) for dp, dn, filenames in os.walk(os.getcwd()) for f in filenames if os.path.splitext(f)[1] == '.out']
#benchmarks = [ f.split('/')[1] for f in benchmarks]

benchmarks = sorted(benchmarks)

content=[]

for benchmark in benchmarks:
  file=open(benchmark)  
  file_content=[line for line in file]
  file_content.append(FILE_SEPARATOR)
  content+=file_content

print 'Read ',len(content),' lines'


#plot_depth(content)

build_constraints_table(content)

build_performance_table(content)
