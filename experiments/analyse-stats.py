#!/usr/bin/python

import os
import sys
import re

import csv

benchmark_name=re.compile('Parsing')
cat=re.compile('Nr of thread categories')

def package(path):
  path_vec=path.split('/')
  package=path_vec[1]
  return package

FILE_SEPARATOR="%===%"


file_sep=re.compile(FILE_SEPARATOR)

po_total_str="Partial order constraints used:"
po_total=re.compile(po_total_str)

po_cons_str="Partial order constraint sizes:"
po_cons=re.compile(po_cons_str)

fr=re.compile("fr:")             # fr: 292/1609
po=re.compile("po:")            # po: 105/105
rf=re.compile("rf:")             # rf: 171/238
rf_order=re.compile("rf-order:") # rf-order 128/238
rf_some=re.compile("rf-some:")   # rf-some: 44/53
rfi=re.compile("rfi:")           # rfi: 35/49
thread_spawn=re.compile("thread-spawn:") # thread-spawn: 3/3
ws_ext=re.compile("ws-ext:")      # ws-ext: 84/152


total_time=re.compile("Time:")
sat_time=re.compile("Runtime decision procedure: ")
iterations=re.compile("Total iterations: ")
vars=re.compile("variables")

def read_tuple(str):
  cols=str.split("/")
  return (int(cols[0]),int(cols[1]))

# statistics about constraints used 
def build_constraints_table(content):
  depth=re.compile('Depth:')

  report_file=open("constraints.csv", 'wb')
  report = csv.writer(report_file, delimiter='&', lineterminator="\\\\\r\n",
                              quotechar=' ', quoting=csv.QUOTE_MINIMAL)   
  depth_list=[]
  benchmark=""

  po_total_val=""
  fr_val=""
  po_val=""
  rf_val=""
  rf_order_val=""
  rf_some_val=""
  rfi_val=""
  thread_spawn_val=""
  ws_ext_val=""

  po_total_cl=""
  fr_cl=""
  po_cl=""
  rf_cl=""
  rf_order_cl=""
  rf_some_cl=""
  rfi_cl=""
  thread_spawn_cl=""
  ws_ext_cl=""

  po_total_vars=""
  fr_vars=""
  po_vars=""
  rf_vars=""
  rf_order_vars=""
  rf_some_vars=""
  rfi_vars=""
  thread_spawn_vars=""
  ws_ext_vars=""
  
  report.writerow(
   ["benchmark",
    " total ",
    " total-v ",
    " total-c ",
    " fr ",
    " fr-v ",
    " fr-c ",
    " po ",
    " po-v ",
    " po-c ",
    " rf ",
    " rf-v ",
    " rf-c ",
    " rf-order ",
    " rf-order-v ",
    " rf-order-c ",
    " rf-some ",
    " rf-some-v ",
    " rf-some-c ",
    " rfi ",
    " rfi-v ",
    " rfi-c ",
    " thread-spawn ",
    " thread-spawn-v ",
    " thread-spawn-c ",
    " ws-ext ",
    " ws-ext-v ",
    " ws-ext-c "
    ])

  for line in content:
    cols=line.split()  

    if benchmark_name.search(line):
      benchmark=cols[1]
      pos=benchmark.rfind("/")
      benchmark=benchmark[pos+1:]
      benchmark=benchmark.replace('_', '\_')

    if(po_total_val=="" and vars.search(line)):
      po_total_vars=cols[0]
      po_total_cl=cols[2]      


    if ws_ext_val!="" and fr.search(line):
      fr_cl=cols[1]
      fr_vars=cols[3]

    if ws_ext_val!="" and po.search(line):
      po_cl=cols[1]
      po_vars=cols[3]

    if ws_ext_val!="" and rf.search(line):
      rf_cl=cols[1]
      rf_vars=cols[3]

    if ws_ext_val!="" and rf_order.search(line):
      rf_order_cl=cols[1]
      rf_order_vars=cols[3]

    if ws_ext_val!="" and rf_some.search(line):
      rf_some_cl=cols[1]
      rf_some_vars=cols[3]

    if ws_ext_val!="" and rfi.search(line):
      rfi_cl=cols[1]
      rfi_vars=cols[3]
    
    if ws_ext_val!="" and thread_spawn.search(line):
      thread_spawn_cl=cols[1]
      thread_spawn_vars=cols[3]

    if ws_ext_val!="" and ws_ext.search(line):
      ws_ext_cl=cols[1]
      ws_ext_vars=cols[3]


    if po_total.search(line):
      po_total_val=read_tuple(cols[4])[1]

    if ws_ext_val=="" and fr.search(line):
      fr_val=read_tuple(cols[1])[1]

    if ws_ext_val=="" and po.search(line):
      po_val=read_tuple(cols[1])[1]

    if ws_ext_val=="" and rf.search(line):
      rf_val=read_tuple(cols[1])[1]

    if ws_ext_val=="" and rf_order.search(line):
      rf_order_val=read_tuple(cols[1])[1]

    if ws_ext_val=="" and rf_some.search(line):
      rf_some_val=read_tuple(cols[1])[1]

    if ws_ext_val=="" and rfi.search(line):
      rfi_val=read_tuple(cols[1])[1]
    
    if ws_ext_val=="" and thread_spawn.search(line):
      thread_spawn_val=read_tuple(cols[1])[1]

    if ws_ext_val=="" and ws_ext.search(line):
      ws_ext_val=read_tuple(cols[1])[1]

      
    if file_sep.search(line):
      report.writerow(
        [benchmark,
         po_total_val,
         po_total_vars,
         po_total_cl,
         fr_val,
         fr_vars,
         fr_cl,
         po_val,
         po_vars,
         po_cl,
         rf_val,
         rf_vars,
         rf_cl,
         rf_order_val,
         rf_order_vars,
         rf_order_cl,
         rf_some_val,
         rf_some_vars,
         rf_some_cl,
         rfi_val,
         rfi_vars,
         rfi_cl,
         thread_spawn_val,
         thread_spawn_vars,
         thread_spawn_cl,
         ws_ext_val,
         ws_ext_vars,
         ws_ext_cl
         ])
      po_total_val=""
      ws_ext_val=""



benchmarks = [os.path.join(dp, f) for dp, dn, filenames in os.walk(os.getcwd()) for f in filenames if os.path.splitext(f)[1] == '.out']

benchmarks = sorted(benchmarks)

content=[]

for benchmark in benchmarks:
  file=open(benchmark)  
  file_content=[line for line in file]
  file_content.append(FILE_SEPARATOR)
  content+=file_content

print 'Read ',len(content),' lines'

build_constraints_table(content)

