#!/bin/bash

# Create an archive of files to run on a remote machine

set -e
set -v

mkdir scratch
cd scratch

diff ../dkr11.cs.ox.ac.uk.config ../dkr12.cs.ox.ac.uk.config > /dev/null 2>&1

cp ~/bin/goto-instrument .
cp ~/bin/goto-cc .
cp ~/bin/lwmusketeer-bonn
cp ~/bin/lwmusketeer_safe-bonn
cp ~/bin/musketeer-bonn

cp ~/t/cbmc-trunk/src/ansi-c/library/pthread_lib.c .
cp ~/t/cbmc-rev2703/src/ansi-c/library/pthread_lib.c pthread_lib_alt.c

cp ../pkgs-jenkins-all .
cp ../pkgs-jenkins-paper .
cp ../pkgs-qm-all .
cp ../pkgs-qm-paper .

cp ../goto-runner.sh .
cp ../cleanup.sh .
cp ../pack.sh .
cp ../clpc11.cs.ox.ac.uk.config .
cp ../dkr11.cs.ox.ac.uk.config .
cp ../dkr12.cs.ox.ac.uk.config .
cp ../snow.config .

tar -cf ../arch.tar *

cd ..
rm -rf scratch

