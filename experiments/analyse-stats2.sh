#!/bin/bash

for m in sc tso pso
do
rm -f ratios-${m}.txt
for d in pthread-atomic pthread wmm-core
do
cd ${d}/logcbmc-mus2-${m}
../../analyse-stats2.py
cp ratios.csv _ratios.csv
[ ${d} != "pthread-atomic" ] && (tail -n +2 ratios.csv > _ratios.csv)
cat _ratios.csv >> ../../ratios-${m}.txt
rm _ratios.csv
cd ../..
done
sed -i.bak -E 's/s&/\t/g' ratios-${m}.txt
sed -i.bak -E 's/&/\t/g' ratios-${m}.txt
sed -i.bak -E 's/\\//g' ratios-${m}.txt
done

rm *.bak
