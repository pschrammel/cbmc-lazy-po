#!/bin/bash

rm -f constraints-stats-sc.txt
cd pthread-atomic/logcbmc-stats-sc
../../analyse-stats.py
cat constraints.csv >> ../../constraints-stats-sc.txt
cd ../..
cd pthread/logcbmc-stats-sc
../../analyse-stats.py
tail -n +2 constraints.csv > constraints2.csv
cat constraints2.csv >> ../../constraints-stats-sc.txt
rm constraints2.csv
cd ../..
cd wmm-core/logcbmc-stats-sc
../../analyse-stats.py
tail -n +2 constraints.csv > constraints2.csv
cat constraints2.csv >> ../../constraints-stats-sc.txt
rm constraints2.csv
cd ../..
sed -i.bak -E 's/&/\t/g' constraints-stats-sc.txt
sed -i.bak -E 's/\\//g' constraints-stats-sc.txt

rm -f constraints-stats-tso.txt
cd pthread-atomic/logcbmc-stats-tso
../../analyse-stats.py
cat constraints.csv >> ../../constraints-stats-tso.txt
cd ../..
cd pthread/logcbmc-stats-tso
../../analyse-stats.py
tail -n +2 constraints.csv > constraints2.csv
cat constraints2.csv >> ../../constraints-stats-tso.txt
rm constraints2.csv
cd ../..
cd wmm-core/logcbmc-stats-tso
../../analyse-stats.py
tail -n +2 constraints.csv > constraints2.csv
cat constraints2.csv >> ../../constraints-stats-tso.txt
rm constraints2.csv
cd ../..
sed -i.bak -E 's/&/\t/g' constraints-stats-tso.txt
sed -i.bak -E 's/\\//g' constraints-stats-tso.txt

rm -f constraints-stats-pso.txt
cd pthread-atomic/logcbmc-stats-pso
../../analyse-stats.py
cat constraints.csv >> ../../constraints-stats-pso.txt
cd ../..
cd pthread/logcbmc-stats-pso
../../analyse-stats.py
tail -n +2 constraints.csv > constraints2.csv
cat constraints2.csv >> ../../constraints-stats-pso.txt
rm constraints2.csv
cd ../..
cd wmm-core/logcbmc-stats-pso
../../analyse-stats.py
tail -n +2 constraints.csv > constraints2.csv
cat constraints2.csv >> ../../constraints-stats-pso.txt
rm constraints2.csv
cd ../..
sed -i.bak -E 's/&/\t/g' constraints-stats-pso.txt
sed -i.bak -E 's/\\//g' constraints-stats-pso.txt


rm *.bak
