Compilation: 

A. without MUSer2:

1. cd cbmc-lazy-po/trunk/src

2. remove -DHAVE_MUSER2 flag in config.inc

3. make minisat2-download

4. make


B. with MUSer2

1. clone https://bitbucket.org/arieg/muser2/

2. apply patch in cbmc-lazy-po/trunk/scripts/muser2.patch to muser2/src

3. cd tools/muser2; make

4. cd api; make

5. cp api/\*.hh api/\*.o \*/\*.a cbmc-lazy-po/trunk/muser2/

6. cd cbmc-lazy-po/trunk/src

7. make

Usage:

option --lazy-encoding enables lazy partial order encoding